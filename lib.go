package gomodc

import "gitlab.com/DmitryGuryanov/gomoda"

func Get() string {
	return "c0.0.1 - " + gomoda.Get()
}
